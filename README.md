# README #
The script requires numpy and python3 (tested on 3.6). For the visual studio project, my install has Anaconda 4.4.

### What is this repository for? ###

* Quick summary
Here is a proof of concept of an idea I had. Isometric tiles on a 2d map imply a 3d structure. Can I solve for that 3d structure by assigning each tile some equations about its individual shape, throwing all the tiles into a matrix, then finding a solution with least squares regression? 

* Version
1.0

### Algorithm Summary
I have defined a few different tile shapes, like flat grass, walls, corners, etc that are represented in the 2d tiles of Oracle of Seasons. To represent the shape of each tile, I use 12 equations that specify the relative xyz of each corner of the tile. 

The 3d structure is the solution to a system of equations that simultaneously states that adjacent corners of tiles should be equal and that all tiles maintain their desired shape. The system is overdetermined, but least squares regression provides an optimal solution.

### Results
I've rendered some images. The first shows different settings for steepness of mountains and strictness of attaching corners. https://imgur.com/a/4KtLsYL

### Image Credits ###
World maps of Holodrum come from TerraEsperZ at VGMaps.com and HylianKing at zelda.gamepedia.com .