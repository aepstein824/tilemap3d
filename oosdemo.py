import numpy as np
import numpy.matlib
from collections import namedtuple
import itertools
import time

UNIT = 1

# Use Unity's left handed coordinate system:
#  y points up, x points east, z points north

#change as we move clockwise
Connection = namedtuple('Connection', ['dx', 'wx', 'dy', 'wy', 'dz', 'wz'])
#clockwise
Tile = namedtuple('Tile', ['t', 'r', 'b', 'l', 'a'])
Corners = namedtuple('Corners', ['tr', 'br', 'bl', 'tl',])

def axis_con(dkey, dval, wkey, wval, od=0, ow=None):
    if ow == None:
        ow = wval
    others = Connection(dx=od, wx=ow, dy=od, wy=ow, dz=od, wz=ow)
    fixed = others._replace(**{dkey : dval, wkey : wval})
    return fixed
def sym_corners(w):
    return Corners(w, w, w, w)
def flat_tile (w):
    top = axis_con('dx', UNIT, 'wx', w)
    right = axis_con('dz', - UNIT, 'wz', w)
    bottom = axis_con('dx', - UNIT, 'wx', w)
    left = axis_con('dz', UNIT, 'wz', w)
    attaches = sym_corners(w)
    return Tile(t=top, r=right, b=bottom, l=left, a=attaches)
def rect_heights_a(westeast, northsouth, heights, wx, wy, wz, wa):
    top = Connection(dx=westeast, wx=wx, dy=(heights.tr - heights.tl), wy=wy, dz=0, wz=wz)
    right = Connection(dx=0, wx=wx, dy=(heights.br - heights.tr), wy=wy, dz=-northsouth, wz=wz)
    bottom = Connection(dx=-westeast, wx=wx, dy=(heights.bl - heights.br), wy=wy, dz=0, wz=wz)
    left = Connection(dx=0, wx=wx, dy=(heights.tl - heights.bl), wy=wy, dz=northsouth, wz=wz)
    attaches = wa
    return Tile(t=top, r=right, b=bottom, l=left, a=attaches)
def rect_heights(westeast, northsouth, heights, wx, wy, wz, wa):
    return rect_heights_a(westeast, northsouth, heights, wx, wy, wz, sym_corners(wa))
grass = flat_tile(1)
picky_grass = flat_tile(3)
stairs = rect_heights(1, 1,  Corners(tl=1, tr=1, bl=0, br=0), 2, 4, 2, 4)
wall_shrink = 0.6 
loosely = 0.1
front_wall = rect_heights(1, wall_shrink, Corners(tl=1, tr=1, bl=0, br=0), 2, 2, 2, 2)
right_wall = rect_heights(wall_shrink, 1, Corners(tl=1, tr=0, bl=1, br=0), 2, loosely, 2, 10)
back_wall = rect_heights(1, wall_shrink, Corners(tl=0, tr=0, bl=1, br=1), 2, loosely, 2, 10)
left_wall = rect_heights(wall_shrink, 1, Corners(tl=0, tr=1, bl=0, br=1), 2, loosely, 2, 10)
tr_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=0,tr=0,bl=1,br=0), loosely, loosely, loosely, 10)
br_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=1,tr=0,bl=0,br=0), loosely, loosely, loosely, 10)
bl_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=0,tr=1,bl=0,br=0), loosely, loosely, loosely, 10)
tl_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=0,tr=0,bl=0,br=1), loosely, loosely, loosely, 10)
bl_concave_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=1,tr=1,bl=0,br=1), 
                               loosely, loosely, loosely, 10)
br_concave_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=1,tr=1,bl=1,br=0), 
                               loosely, loosely, loosely, 10)
tl_concave_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=0,tr=1,bl=1,br=1), 
                               loosely, loosely, loosely, 10)
tr_concave_sticky=rect_heights(wall_shrink, wall_shrink, Corners(tl=1,tr=0,bl=1,br=1), 
                               loosely, loosely, loosely, 10)
tr_loose=rect_heights_a(wall_shrink, wall_shrink, Corners(tl=0,tr=0,bl=1,br=0), 2, 2, 2, 
                        Corners(tl=loosely, tr=loosely, bl=2, br=loosely))
br_loose=rect_heights_a(wall_shrink, wall_shrink, Corners(tl=1,tr=0,bl=0,br=0), 2, 2, 2, 
                        Corners(tl=2, tr=loosely, bl=2, br=loosely))
bl_loose=rect_heights_a(wall_shrink, wall_shrink, Corners(tl=0,tr=1,bl=0,br=0), 2, 2, 2, 
                        Corners(tl=loosely, tr=2, bl=loosely, br=2))
tl_loose=rect_heights_a(wall_shrink, wall_shrink, Corners(tl=0,tr=0,bl=0,br=1), 2, 2, 2,
                        Corners(tl=loosely, tr=loosely, bl=loosely, br=2))
front_left_loose  = rect_heights_a(wall_shrink, wall_shrink, Corners(tl=1,tr=1,bl=0,br=0), 2, 2, 2, 
                        Corners(tl=loosely, tr=2, bl=loosely, br=2)) 
front_right_loose = rect_heights_a(wall_shrink, wall_shrink, Corners(tl=1,tr=1,bl=0,br=0), 2, 2, 2, 
                        Corners(tl=2, tr=loosely, bl=2, br=loosely)) 
lobby_palette = (
    grass,             # 0
    picky_grass,       # 1
    front_wall,        # 2
    right_wall,        # 3
    left_wall,         # 4
    br_loose,          # 5
    bl_loose,          # 6
    br_concave_sticky, # 7 
    bl_concave_sticky, # 8
    tr_loose,          # 9
    tl_loose,          #10
    back_wall,         #11
    front_right_loose, #12
    front_left_loose,  #13
    tr_concave_sticky, #14
    tl_concave_sticky, #15
    )
    
windmill = (
    ( 0,  0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0,  1,  1,  1,  4),
    ( 0,  0,  1, 10, 11, 11, 11, 11, 11, 11,  9,  1,  1,  0, 10, 11,  9,  4),
    ( 0,  1, 10, 11, 11,  9,  0,  0,  0,  0,  3,  1,  0,  0,  4,  1,  3,  4),
    ( 0,  1,  4,  0,  0,  3,  0,  0,  0,  0,  3,  1,  1,  1,  4,  1,  3,  4),
    ( 0,  1,  4,  0,  0, 14,  9,  0,  0,  0,  3,  1,  1,  0,  4,  1,  3,  4),
    ( 0,  1,  4,  0,  0,  0,  3,  0,  0,  7,  5,  1,  1,  0,  6,  2,  5,  4),
    ( 0,  1,  4,  1,  0,  0,  3,  0,  0,  3, 12,  1,  1,  1, 13,  2, 12,  4),
    ( 0,  1,  6,  2,  2,  2,  5,  0,  0,  3,  1,  1,  1,  0,  0,  1,  1,  4),
    ( 0,  1, 13,  2,  2,  2, 12,  0,  0,  3,  1,  1,  1,  0,  0,  0,  0,  4),
    ( 0,  1, 13,  2,  4,  0,  0,  0,  0,  3,  1,  1,  0,  0,  0,  0,  0,  4),
    ( 0,  0,  1,  1,  4,  0,  0,  0,  0,  3,  1,  1,  0,  0,  1,  0,  1,  4),
    ( 0,  0,  0,  1,  6,  2,  2,  2,  2,  5,  1,  1,  0,  0,  0,  1,  0,  4),
    ( 0,  0,  0,  1, 13,  2,  2,  2,  2, 12,  1,  0,  0,  0,  0,  0,  0,  4),
    ( 0,  0,  0,  0,  1,  1,  1,  1,  1,  1,  0,  0,  1,  0,  1,  1,  1,  4),
    ( 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  1,  1,  1,  4),
    ( 1,  1,  1,  1,  1,  1, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 15),
    )[::-1]

def dimensions_for_map(section):
    return len(section[1]), len(section) 

lobby_map = (
  (1, 1, 1,  1, 0, 0, 1,  1, 1, 1),
  (0, 0, 0,  0, 0, 0, 0,  0, 0, 0),
  (1, 1, 1,  1, 1, 1, 1,  1, 1, 1),
  (0, 0, 2,  8, 0, 1, 0,  7, 2, 0),
  (0, 0, 0,  6, 2, 2, 2,  5, 0, 0),
  (0, 0, 0, 13, 2, 2, 2, 12, 0, 0),
  (0, 0, 0,  0, 0, 1, 0,  0, 0, 0),
  (1, 0, 0,  0, 0, 0, 0,  0, 0, 1),
  )#[::-1] #hard coded in top down, need increasing z

really_large_hill = (
    (0, 0, 0,  0,  0,  0, 0, 0),
    (0, 0, 0,  1,  1,  1, 1, 0),
    (0, 0, 0, 10, 11,  9, 1, 0),
    (0, 0, 0,  4,  0,  3, 1, 0),
    (0, 0, 0,  6,  2,  5, 1, 0),
    (0, 0, 0, 13,  2, 12, 1, 0),
    (0, 0, 0, 13,  2, 12, 1, 0),
    (0, 0, 0, 13,  2, 12, 1, 0),
    (0, 0, 0,  0,  0,  1, 1, 0),)[::-1]



  
def iterate2d(w, h):
  return itertools.product(range(w), range(h))

def var_ind_generator(section_w, section_h):
    tile_to_corner_ind = {}
    for i, (x, z) in enumerate(iterate2d(section_w, section_h)):
      fourI = i * 4
      tile_to_corner_ind[(x, z)] = Corners(*range(fourI, fourI + 4))
    return (lambda p, c, d: tile_to_corner_ind[p][c] * 3 + d), tile_to_corner_ind


relative_attach_weight = 1.5

# set xyz of bottom left to 0
origin = (0,0)
origin_weight = 1000

weight_combiner = min

'''
variables:
  xyz for each corner of each tile 
equations:
  set the origin to 0
  for each tile, need 12 equations
  for each corner, need 1 or 4
'''
def equations_for_section(section, palette, section_w, section_h):
    tiles_count = section_w * section_h
    var_count = 3 * tiles_count * 4
    corner_equations_count = (section_w + 1) * (section_h + 1) * 3 * 3
    equation_count = var_count + corner_equations_count
    var_ind, __ = var_ind_generator(section_w, section_h)
    equations = np.matlib.zeros((equation_count, var_count))
    answers = np.matlib.zeros((equation_count, 1))
    weights = np.zeros((equation_count))

    # corner attach equations
    for i, (x, y) in enumerate(iterate2d(section_w + 1, section_h +1)):
        def id(i, d, j):
            return i * 9 + d * 3 + j
        side_edge = x == 0 or x == section_w
        vert_edge = y == 0 or y == section_h
        for d in range(3):
            loc = (x, y)
            # for all of the below, the answer is 0, already initialized
            if side_edge and vert_edge:
                weight_combined = origin_weight
                #really just hammer home the bottom left corner
                equations[id(i, d, 0), var_ind(origin, 2, d)] = 1
                #redundant, keep for symmetry
                equations[id(i, d, 1), var_ind(origin, 2, d)] = 1
                equations[id(i, d, 2), var_ind(origin, 2, d)] = 1
            if not side_edge and not vert_edge:
                equations[id(i, d, 0), var_ind((x-1, y-1), 0, d)] = 1 
                equations[id(i, d, 0), var_ind((x-1,   y), 1, d)] = -1
                equations[id(i, d, 0), var_ind((  x,   y), 2, d)] = 1
                equations[id(i, d, 0), var_ind((  x, y-1), 3, d)] = -1
                equations[id(i, d, 1), var_ind((x-1, y-1), 0, d)] = 1 
                equations[id(i, d, 1), var_ind((x-1,   y), 1, d)] = 1
                equations[id(i, d, 1), var_ind((  x,   y), 2, d)] = -1
                equations[id(i, d, 1), var_ind((  x, y-1), 3, d)] = -1
                equations[id(i, d, 2), var_ind((x-1, y-1), 0, d)] = 1 
                equations[id(i, d, 2), var_ind((x-1,   y), 1, d)] = -1
                equations[id(i, d, 2), var_ind((  x,   y), 2, d)] = -1
                equations[id(i, d, 2), var_ind((  x, y-1), 3, d)] = 1
                weight_combined = weight_combiner(
                    palette[section[y-1][x-1]].a.tr,
                    palette[section[  y][x-1]].a.br,
                    palette[section[  y][  x]].a.bl,
                    palette[section[y-1][  x]].a.tl
                    )
            if not side_edge and vert_edge:
                l, r, bounds = (1, 2, 0) if y == 0 else (0, 3, -1)  
                weight_combined = weight_combiner(
                    palette[section[y+bounds][x-1]].a[l],
                    palette[section[y+bounds][  x]].a[r]
                    )
                equations[id(i, d, 0), var_ind((x-1, y+bounds), l, d)] = 1
                equations[id(i, d, 0), var_ind((  x, y+bounds), r, d)] = -1
                # redundant
                equations[id(i, d, 1), var_ind((x-1, y+bounds), l, d)] = -1
                equations[id(i, d, 1), var_ind((  x, y+bounds), r, d)] = 1            
                equations[id(i, d, 2), var_ind((x-1, y+bounds), l, d)] = 1
                equations[id(i, d, 2), var_ind((  x, y+bounds), r, d)] = -1
            if side_edge and not vert_edge:
                b, t, bounds = (3, 2, 0) if x == 0 else (0, 1, -1)
                weight_combined = weight_combiner(
                    palette[section[y-1][x+bounds]].a[b],
                    palette[section[  y][x+bounds]].a[t]
                    )
                equations[id(i, d, 0), var_ind((x+bounds, y-1), b, d)] = 1
                equations[id(i, d, 0), var_ind((x+bounds,   y), t, d)] = -1
                # redundant
                equations[id(i, d, 1), var_ind((x+bounds, y-1), b, d)] = -1
                equations[id(i, d, 1), var_ind((x+bounds,   y), t, d)] = 1            
                equations[id(i, d, 2), var_ind((x+bounds, y-1), b, d)] = 1
                equations[id(i, d, 2), var_ind((x+bounds,   y), t, d)] = -1
            weights[id(i, d, 0)] = weight_combined * relative_attach_weight
            weights[id(i, d, 1)] = weight_combined * relative_attach_weight
            weights[id(i, d, 2)] = weight_combined * relative_attach_weight
    # tile equations
    for i, (x, y) in enumerate(iterate2d(section_w, section_h)):
        def equation_id(i, j):
            return corner_equations_count + (i * 12 + j)
        loc = (x, y)
        t = palette[section[y][x]]
        for j, (connection, dimension) in enumerate(iterate2d(4, 3)):
            other_corner = (connection - 1) % 4
            id = equation_id(i, j)
            equations[id, var_ind(loc, connection, dimension)] = 1
            equations[id, var_ind(loc, other_corner, dimension)] = -1
            answers[id] = t[connection][dimension * 2]
            weights[id] = t[connection][dimension * 2 + 1]
    return equations, answers, weights

def solve_equations(equations, answers, weights):
    diag_weights = np.matlib.diag(weights)
    start_time = time.time()
    left = diag_weights * equations 
    eq_mult_time = time.time()
    right = diag_weights * answers 
    ans_mult_time = time.time()
    vars, residuals, __, __ = np.linalg.lstsq(left, right)
    solve_time = time.time()
    varsA = vars.getA1()

    print("eq_time {}\nans_time {}\nsolve_time {}".format(
        eq_mult_time - start_time, ans_mult_time - eq_mult_time, solve_time - ans_mult_time))
    return varsA

mtl_format = '''
newmtl {0} 
Ka 1.000 1.000 1.000
Kd 1.000 1.000 1.000
Ks 0.000 0.000 0.000
d 1.0
illum 2
map_Ka {0}.png
map_Kd {0}.png 
'''

def render_to_files(name, varsA, section_w, section_h):
    var_ind, tile_to_corner_ind = var_ind_generator(section_w, section_h)
    tiles_count = section_w * section_h
    var_count = 3 * tiles_count * 4  
    with open(name + ".obj", 'w') as f:
        f.write("mtllib " + name + ".mtl\n")
        f.write("usemtl " + name + "\n")
        def loc_to_uv(x, y):
            return y * (section_w + 1) + x
        for i in range(0, var_count, 3):
            f.write("v {:f} {:f} {:f}\n".format(*varsA[i:i+3]))
        for y, x in iterate2d(section_h + 1, section_w + 1):
            f.write("vt {} {}\n".format(x / section_w, y / section_h))
        for x, y in iterate2d(section_w, section_h):
            x_room = not x == (section_w - 1)
            y_room = not y == (section_h - 1)
            # main faces
            f.write("f {}/{} {}/{} {}/{} {}/{}\n".format(
                    tile_to_corner_ind[(x, y)][0] + 1, loc_to_uv(x+1, y+1) + 1,
                    tile_to_corner_ind[(x, y)][1] + 1, loc_to_uv(x+1,   y) + 1,
                    tile_to_corner_ind[(x, y)][2] + 1, loc_to_uv(  x,   y) + 1,
                    tile_to_corner_ind[(x, y)][3] + 1, loc_to_uv(  x, y+1) + 1))
            #r face
            if x_room:
                f.write("f {}/{} {}/{} {}/{} {}/{}\n".format(
                        tile_to_corner_ind[(x+1, y)][3] + 1, loc_to_uv(x+1, y+1) + 1,    
                        tile_to_corner_ind[(x+1, y)][2] + 1, loc_to_uv(x+1,   y) + 1,
                        tile_to_corner_ind[(  x, y)][1] + 1, loc_to_uv(x+1,   y) + 1,
                        tile_to_corner_ind[(  x, y)][0] + 1, loc_to_uv(x+1, y+1) + 1,
                        ))
            #t face
            if y_room:
                f.write("f {}/{} {}/{} {}/{} {}/{}\n".format(
                        tile_to_corner_ind[(x, y+1)][1] + 1, loc_to_uv(x+1, y+1) + 1,    
                        tile_to_corner_ind[(x,   y)][0] + 1, loc_to_uv(x+1, y+1) + 1,
                        tile_to_corner_ind[(x,   y)][3] + 1, loc_to_uv(  x, y+1) + 1,
                        tile_to_corner_ind[(x, y+1)][2] + 1, loc_to_uv(  x, y+1) + 1,
                        ))
            # corner
            if x_room and y_room:
                f.write("f {}/{} {}/{} {}/{} {}/{}\n".format(
                        tile_to_corner_ind[(x+1, y+1)][2] + 1, loc_to_uv(x+1, y+1) + 1,
                        tile_to_corner_ind[(x+1,   y)][3] + 1, loc_to_uv(x+1, y+1) + 1,
                        tile_to_corner_ind[(  x,   y)][0] + 1, loc_to_uv(x+1, y+1) + 1,
                        tile_to_corner_ind[(  x, y+1)][1] + 1, loc_to_uv(x+1, y+1) + 1,    
                        ))
    with open(name + ".mtl", 'w') as f:
        f.write(mtl_format.format(name))

def process_map(name, section, palette):
    section_w, section_h = dimensions_for_map(section)
    equations, answers, weights = equations_for_section(section, palette, section_w, section_h)
    varsA = solve_equations(equations, answers, weights)
    render_to_files(name, varsA, section_w, section_h)

process_map("windmill", windmill, lobby_palette)
process_map("tall_hill", really_large_hill, lobby_palette)
process_map("horon_lobby", lobby_map, lobby_palette)